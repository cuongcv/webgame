package com.jx.web.controller;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jx.web.model.Account;
import com.jx.web.service.AccountService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	private AccountService accountService;
	
	@Autowired(required = true)
	@Qualifier(value = "accountService")
	private void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Date date = new Date();
		
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return new ModelAndView("login");
	}
	
	@RequestMapping("/save")
	public ModelAndView saveAccount(@Valid @ModelAttribute("acountForm") Account account, Model model,BindingResult bidingresult ) {
		try {
			if(bidingresult.hasErrors()) {
				model.addAttribute("error", "can not add acount");
				return new ModelAndView("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("error", "add user success");
		return new ModelAndView("redirect:/");
	}
	
	@RequestMapping(value = "/listAccount", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("account", new Account());
		model.addAttribute("listAccount", this.accountService.listAccount());
		return "list_user";
	}
	
}
