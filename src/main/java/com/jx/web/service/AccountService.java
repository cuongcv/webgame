/**
 * 
 */
package com.jx.web.service;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;


import com.jx.web.dao.AccountDAO;
import com.jx.web.model.Account;

/**
 * @author ADMIN
 *
 */
@Service
@Transactional
public class AccountService {
	private AccountDAO accountDAO;

	public void setAccountDAO(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}
     
	public void addAccount(Account p) {
		this.accountDAO.addAccount(p);
	}

	public void updateAccount(Account p) {
		this.accountDAO.updateAccount(p);
	}

	public List<Account> listAccount() {
		return this.accountDAO.listAccount();
	}

	@Transactional
	public Account getAccountById(int id) {
		return this.accountDAO.getAccountById(id);
	}

	public void removeAccount(int id) {
		this.accountDAO.removeAccount(id);
	}
}
