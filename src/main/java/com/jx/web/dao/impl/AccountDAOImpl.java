/**
 * 
 */
package com.jx.web.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jx.web.dao.AccountDAO;
import com.jx.web.model.Account;
/**
 * @author ADMIN
 *
 */
@Repository
public class AccountDAOImpl implements AccountDAO {
	private static final Logger logger = LoggerFactory.getLogger(AccountDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void addAccount(Account p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void updateAccount(Account p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Person updated successfully, Person Details="+p);
	}


	@Override
	public Account getAccountById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Account p = (Account)session.load(Account.class, new Integer(id));
		logger.info("Person loaded successfully, Person details="+p);
		return p;
	}

	@Override
	public void removeAccount(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Account p = (Account)session.load(Account.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Person deleted successfully, person details="+p);
	}


	@Override
	public List<Account> listAccount() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Account> personsList = session.createQuery("from account").list();
		for(Account p : personsList){
			logger.info("account List::"+p);
		}
		return personsList;
	}

}
