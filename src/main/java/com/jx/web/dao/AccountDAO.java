package com.jx.web.dao;

import java.util.List;

import javax.transaction.Transactional;

import com.jx.web.model.Account;

@Transactional
public interface AccountDAO {
	
	public void addAccount(Account p);

	public void updateAccount(Account p);

	public List<Account> listAccount();

	public Account getAccountById(int id);

	public void removeAccount(int id);

}
