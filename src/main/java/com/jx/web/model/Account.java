/**
 * 
 */
package com.jx.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ADMIN
 *
 */
@Entity
@Table(name = "account")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "email")
	@NotNull(message = "không được b�? trống")
	@Email(message = "Không đúng định dạng email! Vui lòng nhập lại email")
	@Size(min = 1, max = 64, message = "Email độ dài nằm trong khoảng {min} đến {max}")
	private String email;
	
	@Column(name = "username")
	@NotNull(message = "Không được b�? trống")
	@Size(min = 1 ,max = 32, message = "không được b�? trống")
	private String userName;

	@Column(name = "rowpass")
	@NotNull(message = "Không được b�? trống")
	@Size(min = 1,max = 32, message = "không được b�? trống")
	private String rowPass;
	
	@Min(value=1, message="must be equal or greater than 1")  
    @Max(value=999999, message="must be equal or less than 45")  
	private Long coin;
}
